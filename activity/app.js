const express = require('express')
   
const application = express();

application.use(express.json());  
  
const port = 3000; 

application.get('/', (req, res) => {
	res.send('Welcome to the Express App Activity'); 
}); 

let courseList = []; 

application.post('/course', (req, res) => {
	courseList.push(req.body);
	let course = req.body.courseName;
	let message = `Course ${course} successfully added.`;
	res.send(message);
      
}); 

application.get('/courses', (req, res) => {
    res.send(courseList); 
}); 

application.put('/newPrice', (req, res) => {
	let courseRef = req.body.courseName;
	let newPrice = req.body.price;

	for (let counter = 0; counter < courseList.length; counter++){
		if (courseRef === courseList[counter].courseName){
			courseList[counter].price = newPrice;
			message = `Course ${courseRef} price has been updated.`;
			break;
		}else {
          	 message = 'Course does not exist inside the collection'; 
          }
	}

	res.send(message);
})

application.listen(port, () => console.log(`Server is running on ${port}`));
